%% ECON 512A HOMEWORK 1
% SUAT AKBULUT sqa5456@psu.edu
% State College, PA
%% QUESTION 1
% First let us create the $X$ vector.
%% 
X = [1, 1.5, 3.5, 4.78, 5, 7.2, 9, 10, 10.1, 10.56];
%%
% We can define $y1$ and $y2$ functions as follows.
%%
y1 = @(x) -2 + 0.5*x;
y2 = @(x) -2 + 0.5*x.^2;
%%
% Now, we can plot y1 and y2 against X by using the following command
%%
plot(X,y1(X),X,y2(X));
%% QUESTION 2
% In order to generalize the upper and lower bounds for the interval lets define a generic upper and lower bounds, upper and lower, respectively, and the number of rows, rows.
%%
 lower = -20;
 upper = 40;
 rows  = 120;
%%
% Now we can use the following command to create a rowsx1 vector X containing evenly-spaced numbers starting between lower and upper.
%%
step  = (upper-lower)/(rows-1);
X     = lower:step:upper;
sumX  = sum(X);

%% QUESTION 3
% Let us first create the given matrix $A$ and vector $b$.
%%
 A = [3 4 5;2 12 6; 1 7 4];
 b = [3; -2; 10];
%%
% Then we can find matrices $C$, $D$, $E$ and $F$, and the solution to the linear system, $x$, by using the simple matrix operations as follows:
%%
  C = A'*b;
  D = inv(A'*A)*b;
  E = sum(A'*b);
  F = A;
  F(:,[3]) = [];
  F([2],:) = [];
  x = A\b;
%% QUESTION 4
% This is a good example to use the built-in command blkdiagonal
%%
Aedit    = blkdiag(A,A,A,A,A);
%% QUESTION 5
% Just like we did in question 2, let us generalize the mean and standard deviation of the normal distribution to the variables $\mu$ and $\sigma$, respectively.
%%
mu     = 6.5;
sigma  = 3;
%%
% Now we can draw a $5x3$ matrix whose entries are drwan from a normal distribution with mean $\mu$ and standard deviation $\sigma$.
%%
A      = sigma*randn(5, 3)+mu;
%%
% Then for remaining part of the question we can run the following loop.
%%
nr     = size(A,1);
nc     = size(A,2); 
Aedit = A;
for r = 1:nr;
    for c =1:nc;
        if Aedit(r,c)<4;
            Aedit(r,c)=0;
        else 
            Aedit(r,c)=1;
        end
    end
end
disp(A);
disp(Aedit);
%% QUESTION 6
% By usind the command 'importdata', we can import the .csv file to MATLAB. Let MyData be the this matrix.
%%
MyData  = importdata('datahw1.csv');
%%
% Now, let's name each column with its name.
%%
Export  = MyData(:,3);
RD      = MyData(:,4);
prod    = MyData(:,5);
cap     = MyData(:,6);
%%
% Since we have a constant vector in the model, let define that vector, too.
%%
ns      = size(MyData,1);
cons    = ones(ns,1);
%%
% Now we can solve for $\hat{\beta}_{OLS}$ by using the OLS formula.
%%
X       = [cons Export RD cap];
Betahat = inv(X'*X)*X'*prod;
